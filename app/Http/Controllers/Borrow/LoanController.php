<?php

namespace App\Http\Controllers\Borrow;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoanController extends Controller
{
    public function liteLoanIndex(Request $var)
    {
        try {
            return view('/borrow/lite-loan');
        } catch (\Exception $e) {
            return view('errors/500');
        }
    }

    public function personalLoanIndex(Request $var)
    {
        try {
            return view('/borrow/personal-loan');
        } catch (\Exception $e) {
            return view('errors/500');
        }
    }
}
