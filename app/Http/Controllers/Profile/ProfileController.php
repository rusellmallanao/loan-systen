<?php

namespace App\Http\Controllers\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function index(Request $request)
    {
        try {
            return view('profile/index');
        } catch (\Exception $e) {
            return view('errors/500');
        }
    }
}
