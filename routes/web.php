<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/lite-loan', 'Borrow\LoanController@liteLoanIndex');
Route::get('/personal-loan', 'Borrow\LoanController@personalLoanIndex');
Route::get('/salary-loan', 'Borrow\LoanController@personalLoanIndex');
Route::get('/small-business-loan', 'Borrow\LoanController@personalLoanIndex');
Route::get('/business-loan', 'Borrow\LoanController@personalLoanIndex');

Route::get('/profile', 'Profile\ProfileController@index');
