@extends('layouts.app')

@section('content')
    <div class="container">
            <div class="jumbotron">
            <h1 class="display-4">My Profile</h1>
            <hr>
            <div class="col-md-6 col-lg-6 ">
                <form action="/profile/{{\Auth::user()->id}}">
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" id="name"  value="{{\Auth::user()->name}}" name="name">
                </div>
                <div class="form-group">
                    <label for="date">Birth Date:</label>
                    <input type="date" class="form-control" id="date"name="birthDate">
                </div>
                <div class="form-group">
                    <label for="contact">Contact:</label>
                    <input type="number" class="form-control" id="contact"  value="{{\Auth::user()->contact}}" name="contact">
                </div>
                <div class="form-group">
                    <label for="address">Address:</label>
                    <input type="text" class="form-control" id="address"  value="{{\Auth::user()->address}}"name="address">
                </div>
                <div class="form-group">
                    <label for="type-residence">Type of Residency:</label>
                    <input type="text" class="form-control" id="type-residence"  value="{{\Auth::user()->type_residency}}" name="type_residensy">
                </div>
                <div class="form-group">
                    <label for="length-residence">Length of Residency:</label>
                    <input type="number" class="form-control" id="length-residence"  value="{{\Auth::user()->length_residency}}"name="length_residency">
                </div>
                <h4>Work Info</h4>
                <div class="form-group">
                    <label for="company">Company Name:</label>
                    <input type="text" class="form-control" id="company"  value="{{\Auth::user()->company}}"name="company">
                </div>
                <div class="form-group">
                    <label for="position">Position:</label>
                    <input type="text" class="form-control" id="position"  value="{{\Auth::user()->position}}"name="position">
                </div>
                <div class="form-group">
                    <label for="employment-lenght">Emploment Lenght:</label>
                    <input type="number" class="form-control" id="employment-lenght"  value="{{\Auth::user()->employment_lenght}}"name="employment_length">
                </div>
                </form>
            </div>
            <div class="col-md-6 col-lg-6">
                <div class="form-group">
                    <label>Valid ID's</label>
                    <input type="file" class="form-control-file" id="valid-id-one"name="id_one">
                    <input type="file" class="form-control-file" id="valid-id-two"name="id_two">
                </div>
                <div class="form-group">
                    <label for="proff-of-billing">Proff of billing</label>
                    <input type="file" class="form-control-file" id="proff-of-billing"name="proff_billing">
                </div>
                <div class="form-group">
                    <label for="payslip">payslip</label>
                    <input type="file" class="form-control-file" id="payslip"name="payslip">
                </div>
                <div class="form-group">
                    <input type="checkbox" name="is_agree" id="is_agree"> <label for="is_agree"> I Agree</label> <br>
                    <input type="checkbox" name="is_true" id="is_true"> <label for="is_true">I hereby declare all info are truth and valid</label> <br>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>

            </div>
        </div>
    </div>
@endsection
<style>
        .jumbotron {
            min-height: 900px;
            max-height: 900px;
            overflow: scroll;
        }
        .col-md-6 {
            float: left !important;
        }
    </style>
